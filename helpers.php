<?php
defined('_ACCESS') or die('Restricted access');

/*
 * Send Mail with SendGrid
 * https://github.com/sendgrid/sendgrid-php#hello-email
*/
function sendMail() {
  global $config;
  global $client_data;

  $email_status;
  $user = $client_data['contactInfo'];
  $user['fullname'] = $user['firstName'] . ' ' . $user['lastName'];

  $email = new \SendGrid\Mail\Mail();
  $email->setFrom($config->sender_email, $config->sender_name);
  $email->setSubject($config->sender_subject . ' ' . $user['fullname'] );
  $email->addTo($user['email'], $user['fullname']);
  $email->addBcc($config->notification_email, $config->notification_name);
  $email->addContent("text/plain", "Satrack Quotation for " . $user['fullname']);
  $email->addContent("text/html", template($client_data, 'client'));

  $sendgrid = new \SendGrid($config->SENDGRID_API_KEY);

  try {
      $response = $sendgrid->send($email);

      $email_status = [
        'code' => $response->statusCode(),
        'headers' => $response->headers()
      ];

      return $email_status;
  } catch (Exception $e) {
      $email_status = [
        'code' => 'error',
        'message' => $e->getMessage()
      ];
  }

}

/**
 * Email template
*/
function template($data, $template) {
  require "template/$template-template.php";

  return $template;
}

/*
 * Send Mail with SendGrid
 * https://github.com/sendgrid/sendgrid-php#hello-email
*/
function sendNotification() {
  global $config;
  global $client_data;

  $email_status;
  $user = $client_data['contactInfo'];

  $email = new \SendGrid\Mail\Mail();
  $email->setFrom('mercadeo@satrack.com', 'Mercadeo Satrack');
  $email->setSubject($user['firstName'] . '' . $user['lastName'] . ' in Get a Quote App');
  $email->addTo($config->notification_email, $config->notification_name);
  $email->addContent("text/plain", "New register in Get a Quote App");
  $email->addContent(
      "text/html", template($client_data, 'notification')
  );

  $sendgrid = new \SendGrid($config->SENDGRID_API_KEY);

  try {
      $response = $sendgrid->send($email);

      $email_status = [
        'code' => $response->statusCode(),
        'headers' => $response->headers()
      ];

      return $email_status;
  } catch (Exception $e) {
      $email_status = [
        'code' => 'error',
        'message' => $e->getMessage()
      ];
  }

}

/*
 * Set CORS
*/
function enableCORS() {
  header('Access-Control-Allow-Origin: *');
  header("Access-Control-Allow-Headers: Origin, Content-Type, Access-Control-Request-Method");
  header("Access-Control-Allow-Methods: POST");
  header("Allow: POST");
  header("Content-Type: application/json");

  $method = $_SERVER['REQUEST_METHOD'];
  if($method == "OPTIONS") {
      die();
  }
}
