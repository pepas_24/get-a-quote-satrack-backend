<?php
/**
 * Get a Satrack Quotation Backend
 *
 * Sends a email with SendGrid API
 *
 * @author Sebastian Gutierrez
 * @package get-a-quote-backend
 * @since v1.0.0
 *
*/

define('_ACCESS', true);

include 'autoload.php';

enableCORS();

/*
 * Check body data
*/
$client_data = json_decode(file_get_contents('php://input'), true);

if (isset($client_data) && !empty($client_data)) {

  if ($client_data['template_only'] == true) {
    header("Content-Type: text/html");
    echo template($client_data, $client_data['template']);

  } else {
    $response = sendMail();
    sendNotification();
    echo json_encode($response);

  }

} else {
  $response = [
    'code' => '204',
    'message' => 'Required Params not passed',
    'payload' => $client_data
  ];
  echo json_encode($response);
}
