# Código de servidor para cotizador web para Satrack USA

## Iniciar proyecto

Clona este repositorio con el comando `git clone https://github.com/pepas24/get-a-quote-backend.git`

Instala las dependencias del proyecto con el comando:

```bash
composer install
```

Inicia el proyecto en el servidor local con:

```bash
php -S localhost:8000
```

## Generalidades del código

### Dependencias

- Composer para instalar `sendgrid-php`, consulta como instalar [aquí](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-macos)
- Node JS y NPM para correr MJML para las plantillas de correo, consulta como instalar [aquí](https://nodejs.org/es/download/)
- [SendGrid PHP](https://github.com/sendgrid/sendgrid-php) librería para usar SendGrid con PHP
- [MJML](http://mjml.io/) más información en [http://mjml.io/documentation/](http://mjml.io/documentation/)

### Estructura del proyecto

- `env.example.php` Ejemplo de configuración de SendGrid, contiene la llave del api
- `index.php` Recibe los parámetros del cliente y los procesa
- `autoload.php` Carga las variables de entorno la configuración y las funciones de ayuda.
- `config.php` Contiene la configuración para el envío de correos
- `helpers.php` Contiene las funciones para enviar los correos y cargar las plantillas
- `composer.json` Configuración de dependencias de composer
- template
  - `client-template.php` Procesa los datos para la plantilla que se envía al cliente
  - `notification-template.php` Procesa los datos para la platilla que notifica al encargado de Satrack
  - mjml Contiene los archivos .mjml y la salida de estos para las plantillas de correo ver la sección [modificar platillas](#modificar-plantillas)

### Probando el código

La mejor forma de probar los cambios es usar Postman para hacer las solicitudes.

Se debe enviar una petición `POST` con un objeto JSON que contenga la elección del usuario.

Los parámetros `tempalte_only` y `template` se usan para evitar enviar los correos y previsualizar la plantilla como html.

**Ejemplo de solicitud:**

```json
{
  "template_only": true,
  "template": "client",
    "contactInfo": {
        "firstName": "Sebastian",
        "lastName": "Gutierrez",
        "email": "sebastiangutierrezcorrea@gmail.com"
    },
    "deviceInfo": {
        "selectedPlan": {
            "id": "BS_300",
            "name": "Basic Wired",
            "type": "wired",
            "features": [
                "gps",
                "driving"
            ],
            "prices": [
                {
                    "type": "Sale",
                    "length": 0,
                    "monthly_fee": 10,
                    "device": 59.99
                },
                {
                    "type": "Loan",
                    "length": 12,
                    "monthly_fee": 13,
                    "device": 0
                },
                {
                    "type": "Loan",
                    "length": 24,
                    "monthly_fee": 11,
                    "device": 0
                }
            ],
            "price": {
                "type": "Loan",
                "length": 12,
                "monthly_fee": 13,
                "device": 0
            }
        },
        "type": "wired",
        "assetsAmount": "4",
        "customFeature": "",
        "features": [
            "driving"
        ],
        "contractLength": 12
    }
}
```

### Modificar Plantillas

Para modificar las plantillas se debe instalar MJML de forma global con el comando:

```bash
npm install -g mjml
```

Luego ir al directorio `template/mjml` y ejecutar el comando:

```bash
mjml --watch client.mjml -o client.html
```

Consulta la documentación completa [aquí](http://mjml.io/documentation/)

## Publicar Cambios

Para publicar los cambios usa los siguientes comandos:

```bash
# Comprimir archivos
zip -r backend.zip * -x .git .gitignore

# Subir archivo al servidor
scp backend.zip useradmin@40.70.129.14:/home/useradmin

# Ingresa al servidor por ssh
ssh useradmin@40.70.129.14
# Escribir contraseña

# Cambiar de usuario
sudo su - www-data -s /bin/bash

# Ir al directorio donde se subió el archivo
cd /home/useradmin
rm -fr /var/www/html/satrack-com-us2/apps/get-a-quote-backend

# Descomprimir el archivo en el directorio de la aplicación
unzip backend.zip -d /var/www/html/satrack-com-us2/apps/get-a-quote-backend
cd /var/www/html/satrack-com-us2/apps/get-a-quote-backend

# Volver a usuario useradmin
exit

# Eliminar archivo zip
rm backend.zip
exit
```
