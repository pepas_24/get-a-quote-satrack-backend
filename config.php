<?php
defined('_ACCESS') or die('Restricted access');

$config = (object)[
  'SENDGRID_API_KEY' => env('SENDGRID_API_KEY'),
  'sender_name' => 'Clarena Moreno',
  'sender_email' => 'clarena.moreno@satrack.com',
  'notification_name' => 'Info Satrack',
  'notification_email' => 'infogps@satrack.com',
  'sender_subject' => 'Satrack Quotation for',
  // 'tampleteID' => 'd-b333f325673541f49292426335110efe'
];
