
    <!doctype html>
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
        <title>
          
        </title>
        <!--[if !mso]><!-- -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!--<![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style type="text/css">
          #outlook a { padding:0; }
          .ReadMsgBody { width:100%; }
          .ExternalClass { width:100%; }
          .ExternalClass * { line-height:100%; }
          body { margin:0;padding:0;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%; }
          table, td { border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt; }
          img { border:0;height:auto;line-height:100%; outline:none;text-decoration:none;-ms-interpolation-mode:bicubic; }
          p { display:block;margin:13px 0; }
        </style>
        <!--[if !mso]><!-->
        <style type="text/css">
          @media only screen and (max-width:480px) {
            @-ms-viewport { width:320px; }
            @viewport { width:320px; }
          }
        </style>
        <!--<![endif]-->
        <!--[if mso]>
        <xml>
        <o:OfficeDocumentSettings>
          <o:AllowPNG/>
          <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
        </xml>
        <![endif]-->
        <!--[if lte mso 11]>
        <style type="text/css">
          .outlook-group-fix { width:100% !important; }
        </style>
        <![endif]-->
        
        
    <style type="text/css">
      @media only screen and (min-width:480px) {
        .mj-column-per-100 { width:100% !important; max-width: 100%; }
.mj-column-px-232 { width:232px !important; max-width: 232px; }
.mj-column-px-368 { width:368px !important; max-width: 368px; }
      }
    </style>
    
  
        <style type="text/css">
        
        

    @media only screen and (max-width:480px) {
      table.full-width-mobile { width: 100% !important; }
      td.full-width-mobile { width: auto !important; }
    }
  
        </style>
        
        
      </head>
      <body>
        
        
      <div style>
        
      
      <!--[if mso | IE]>
      <table
         align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
      >
        <tr>
          <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
      <![endif]-->
    
      
      <div style="Margin:0px auto;max-width:600px;">
        
        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
          <tbody>
            <tr>
              <td style="color: #495361; direction: ltr; font-size: 0px; padding: 20px 0; padding-bottom: 0; text-align: center; vertical-align: top;" align="center" valign="top">
                <!--[if mso | IE]>
                  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                
        <tr>
      
            <td
               class="" style="vertical-align:top;width:600px;"
            >
          <![endif]-->
            
      <div class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
        
      <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
        
            <tr>
              <td align="left" style="color: #495361; font-size: 0px; padding: 10px 25px; word-break: break-word;">
                
      <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
        <tbody>
          <tr>
            <td style="color: #495361; padding: 8px 0; width: 160px;" width="160">
              
      <img height="auto" src="https://www.satrack.com.co/wp-content/uploads/2018/12/satrack-logo-color.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:16px;" width="160">
    
            </td>
          </tr>
        </tbody>
      </table>
    
              </td>
            </tr>
          
            <tr>
              <td align="left" style="color: #495361; font-size: 0px; padding: 10px 25px; word-break: break-word;">
                
      <div style="font-family:arial;font-size:16px;line-height:1.5;text-align:left;color:#212121;">
        Hello <?= $user_firstname ?> <?= $user_lastname ?>
      </div>
    
              </td>
            </tr>
          
            <tr>
              <td align="left" style="color: #495361; font-size: 0px; padding: 10px 25px; word-break: break-word;">
                
      <div style="font-family:arial;font-size:16px;line-height:1.5;text-align:left;color:#212121;">
        Here you will find the quotation for your <?= $vehicles ?>. If you want to have a live demo of our platform, click on the get a live demo button at the bottom of this email and select your preferred date.
      </div>
    
              </td>
            </tr>
          
            <tr>
              <td align="left" style="color: #495361; font-size: 0px; padding: 10px 25px; word-break: break-word;">
                
      <div style="font-family:arial;font-size:16px;line-height:1.5;text-align:left;color:#212121;">
        Agreement Length: <?= $contract_length ?><br> Device option: <?= $option ?><br> Installation Responsible: To be defined*
      </div>
    
              </td>
            </tr>
          
            <tr>
              <td align="left" class="price-table" style="color: #495361; font-size: 0px; padding: 10px 25px; word-break: break-word;">
                
      <table cellpadding="0" cellspacing="0" width="100%" border="0" style="cellspacing:0;color:#212121;font-family:arial;font-size:16px;line-height:1.5;table-layout:auto;width:100%;">
        <tr style="border-bottom: solid 1px #707070;">
            <th style="text-align: left; padding: 8px 0;" align="left">Item Description</th>
            <th class="tr" style="padding: 8px 0; text-align: right;" align="right">Unit Price</th>
            <th class="tr" style="padding: 8px 0; text-align: right;" align="right">Quantity</th>
            <th class="tr" style="padding: 8px 0; text-align: right;" align="right">Total Amount</th>
          </tr>
          <tr style="border-bottom: solid 1px #707070;">
            <td style="color: #495361; padding: 8px 0;"><?= $device_name ?></td>
            <td class="tr" style="color: #495361; padding: 8px 0; text-align: right;" align="right">$ <?= $device_price ?></td>
            <td class="tr" style="color: #495361; padding: 8px 0; text-align: right;" align="right"><?= $assets_amount ?></td>
            <td class="tr" style="color: #495361; padding: 8px 0; text-align: right;" align="right">$ <?= $device_total ?></td>
          </tr>
          <tr style="border-bottom: solid 1px #707070;">
            <td style="color: #495361; padding: 8px 0;"><?= $plan_name ?> Plan</td>
            <td class="tr" style="color: #495361; padding: 8px 0; text-align: right;" align="right">$ <?= $plan_price ?></td>
            <td class="tr" style="color: #495361; padding: 8px 0; text-align: right;" align="right"><?= $assets_amount ?></td>
            <td class="tr" style="color: #495361; padding: 8px 0; text-align: right;" align="right">$ <?= $plan_total ?></td>
          </tr>
      </table>
    
              </td>
            </tr>
          
      </table>
    
      </div>
    
          <!--[if mso | IE]>
            </td>
          
        </tr>
      
                  </table>
                <![endif]-->
              </td>
            </tr>
          </tbody>
        </table>
        
      </div>
    
      
      <!--[if mso | IE]>
          </td>
        </tr>
      </table>
      
      <table
         align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
      >
        <tr>
          <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
      <![endif]-->
    
      
      <div style="Margin:0px auto;max-width:600px;">
        
        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
          <tbody>
            <tr>
              <td style="color: #495361; direction: ltr; font-size: 0px; padding: 0; text-align: center; vertical-align: top;" align="center" valign="top">
                <!--[if mso | IE]>
                  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                
        <tr>
      
            <td
               class="" style="vertical-align:top;width:232px;"
            >
          <![endif]-->
            
      <div class="mj-column-px-232 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
        
      <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
        
      </table>
    
      </div>
    
          <!--[if mso | IE]>
            </td>
          
            <td
               class="" style="vertical-align:top;width:368px;"
            >
          <![endif]-->
            
      <div class="mj-column-px-368 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
        
      <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
        
            <tr>
              <td align="left" class="price-table" style="color: #495361; font-size: 0px; padding: 10px 25px; word-break: break-word;">
                
      <table cellpadding="0" cellspacing="0" width="100%" border="0" style="cellspacing:0;color:#212121;font-family:arial;font-size:16px;line-height:1.5;table-layout:auto;width:100%;">
        <tr style="border-bottom: solid 1px #707070;">
            <th style="text-align: left; padding: 8px 0;" align="left">Total Monthly Amount</th>
            <td class="tr" style="color: #495361; padding: 8px 0; text-align: right;" align="right">$ <?= $monthly_total ?></td>
          </tr>
          <tr style="border-bottom: solid 1px #707070;">
            <th style="text-align: left; padding: 8px 0;" align="left">Total One Time Amount</th>
            <td class="tr" style="color: #495361; padding: 8px 0; text-align: right;" align="right">$ <?= $one_time_total ?></td>
          </tr>
          <tr style="border-bottom: none;">
            <th style="text-align: left; padding: 8px 0;" align="left">Total Paid <?= $asterisk ?></th>
            <td style="color: #495361; padding: 8px 0; text-align: right;" align="right">$ <?= $quote_total ?></td>
          </tr>
      </table>
    
              </td>
            </tr>
          
      </table>
    
      </div>
    
          <!--[if mso | IE]>
            </td>
          
        </tr>
      
                  </table>
                <![endif]-->
              </td>
            </tr>
          </tbody>
        </table>
        
      </div>
    
      
      <!--[if mso | IE]>
          </td>
        </tr>
      </table>
      
      <table
         align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
      >
        <tr>
          <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
      <![endif]-->
    
      
      <div style="Margin:0px auto;max-width:600px;">
        
        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
          <tbody>
            <tr>
              <td style="color: #495361; direction: ltr; font-size: 0px; padding: 0; text-align: center; vertical-align: top;" align="center" valign="top">
                <!--[if mso | IE]>
                  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                
        <tr>
      
            <td
               class="" style="vertical-align:top;width:600px;"
            >
          <![endif]-->
            
      <div class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
        
      <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
        
            <tr>
              <td align="left" style="color: #495361; font-size: 0px; padding: 10px 25px; word-break: break-word;">
                
      <div style="font-family:arial;font-size:16px;line-height:1.5;text-align:left;color:#212121;">
        <small>
                *Only wired units need device installation. Installation not included, please let us know if you need help with this. <br>
                <?= $asterisk2 ?>
              </small>
      </div>
    
              </td>
            </tr>
          
      </table>
    
      </div>
    
          <!--[if mso | IE]>
            </td>
          
        </tr>
      
                  </table>
                <![endif]-->
              </td>
            </tr>
          </tbody>
        </table>
        
      </div>
    
      
      <!--[if mso | IE]>
          </td>
        </tr>
      </table>
      
      <table
         align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
      >
        <tr>
          <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
      <![endif]-->
    
      
      <div style="Margin:0px auto;max-width:600px;">
        
        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
          <tbody>
            <tr>
              <td style="color: #495361; direction: ltr; font-size: 0px; padding: 20px 0; text-align: center; vertical-align: top;" align="center" valign="top">
                <!--[if mso | IE]>
                  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                
        <tr>
      
            <td
               class="" style="vertical-align:top;width:600px;"
            >
          <![endif]-->
            
      <div class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
        
      <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
        
            <tr>
              <td align="left" style="color: #495361; font-size: 0px; padding: 10px 25px; word-break: break-word;">
                
      <div style="font-family:arial;font-size:16px;line-height:1.5;text-align:left;color:#212121;">
        Check the features all our plans include here: <a href="https://www.satrack.com/pricing/" style="color: #4080f9;">https://www.satrack.com/pricing/</a>
      </div>
    
              </td>
            </tr>
          
            <tr>
              <td align="left" vertical-align="middle" style="color: #495361; font-size: 0px; padding: 10px 25px; word-break: break-word;">
                
      <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:separate;line-height:100%;">
        <tr>
          <td align="center" bgcolor="#4080f9" role="presentation" style="color: #495361; padding: 8px 0; border: none; border-radius: 3px; cursor: auto; mso-padding-alt: 6px 18px; background: #4080f9;" valign="middle">
            <a href="https://calendly.com/clarena_moreno_satrack?ref=get-a-quote" style="display:inline-block;background:#4080f9;color:#ffffff;font-family:arial;font-size:16px;font-weight:normal;line-height:1.5;Margin:0;text-decoration:none;text-transform:none;padding:6px 18px;mso-padding-alt:0px;border-radius:3px;" target="_blank">
              Get a live demo
            </a>
          </td>
        </tr>
      </table>
    
              </td>
            </tr>
          
      </table>
    
      </div>
    
          <!--[if mso | IE]>
            </td>
          
        </tr>
      
                  </table>
                <![endif]-->
              </td>
            </tr>
          </tbody>
        </table>
        
      </div>
    
      
      <!--[if mso | IE]>
          </td>
        </tr>
      </table>
      
      <table
         align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
      >
        <tr>
          <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
      <![endif]-->
    
      
      <div style="Margin:0px auto;max-width:600px;">
        
        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
          <tbody>
            <tr>
              <td style="color: #495361; direction: ltr; font-size: 0px; padding: 0; text-align: center; vertical-align: top;" align="center" valign="top">
                <!--[if mso | IE]>
                  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                
        <tr>
      
            <td
               class="" style="vertical-align:top;width:600px;"
            >
          <![endif]-->
            
      <div class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
        
      <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
        
            <tr>
              <td style="color: #495361; font-size: 0px; padding: 10px 25px; word-break: break-word;">
                
      <p style="border-top:solid 1px #707070;font-size:1;margin:0px auto;width:100%;">
      </p>
      
      <!--[if mso | IE]>
        <table
           align="center" border="0" cellpadding="0" cellspacing="0" style="border-top:solid 1px #707070;font-size:1;margin:0px auto;width:550px;" role="presentation" width="550px"
        >
          <tr>
            <td style="height:0;line-height:0;">
              &nbsp;
            </td>
          </tr>
        </table>
      <![endif]-->
    
    
              </td>
            </tr>
          
            <tr>
              <td align="left" style="color: #495361; font-size: 0px; padding: 10px 25px; word-break: break-word;">
                
      <div style="font-family:arial;font-size:16px;line-height:1.5;text-align:left;color:#212121;">
        Clarena Moreno <br> Director USA <br> 9100 S Dadeland Blvd Suite 1500 <br> Ph: 833-700-8883 <br> Cell: 786 647 2568 <br> www.satrack.com
      </div>
    
              </td>
            </tr>
          
      </table>
    
      </div>
    
          <!--[if mso | IE]>
            </td>
          
        </tr>
      
                  </table>
                <![endif]-->
              </td>
            </tr>
          </tbody>
        </table>
        
      </div>
    
      
      <!--[if mso | IE]>
          </td>
        </tr>
      </table>
      <![endif]-->
    
    
      </div>
    
      </body>
    </html>
  