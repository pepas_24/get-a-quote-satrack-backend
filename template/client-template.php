<?php
defined('_ACCESS') or die('Restricted access');

$user = $data['contactInfo'];
$device = $data['deviceInfo'];

$user_firstname = $user['firstName'];
$user_lastname = $user['lastName'];

$option = $device['selectedPlan']['price']['type'];
$contract_length = $device['contractLength'];

$device_name = $device['selectedPlan']['id'];
$device_price = $device['selectedPlan']['price']['device'];
$assets_amount = $device['assetsAmount'];
$device_total = $device_price * $assets_amount;

$plan_name = $device['selectedPlan']['name'];
$plan_price = $device['selectedPlan']['price']['monthly_fee'];
$plan_total = $plan_price * $assets_amount;

$one_time_total = $device_total;
$monthly_total = $plan_total;
$quote_total = $device_total + $plan_total;

$asterisk = $option === 'Sale' ? '**' : '';
$asterisk2 = $option === 'Sale' ? "**Taxes subject to shipping address." : '';
$vehicles = $assets_amount <= '1' ? "vehicle" : "$assets_amount vehicles";

$has_other_feature = false;

foreach ($device['features'] as $feature) {
  if ($feature === 'other') {
    $has_other_feature = true;
    break;
  }
}

if ($has_other_feature == true) {
  ob_start();
  include __DIR__ . '/mjml/output/client-with-other-feature.php';
  $template = ob_get_clean();
} else {
  ob_start();
  include __DIR__ . '/mjml/output/client.php';
  $template = ob_get_clean();
}
