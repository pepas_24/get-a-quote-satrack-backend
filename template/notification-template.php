<?php
defined('_ACCESS') or die('Restricted access');

$user_firstname = $data['contactInfo']['firstName'];
$user_lastname = $data['contactInfo']['lastName'];
$user_email = $data['contactInfo']['email'];
$assets_amount = $data['deviceInfo']['assetsAmount'];
$contract_length = $data['deviceInfo']['contractLength'];
$features = $data['deviceInfo']['features'];
$option = $data['deviceInfo']['selectedPlan']['price']['type'];
$other_feature = $data['deviceInfo']['customFeature'] ? $data['deviceInfo']['customFeature'] : 'none';

$selected_features = '';
foreach ($features as $feature) {
  $selected_features = $selected_features . $feature . ', ';
}

ob_start();
include __DIR__ . '/mjml/output/notification.php';
$template = ob_get_clean();
